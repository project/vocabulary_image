<?php

/**
 * @file
 * Administrative settings for image validation.
 */

/**
 * Helper function for image validation admin configuration.
 */
function vocabulary_image_settings_form($form, &$form_state) {
  // Initializes form with common settings.
  $form['vocabulary'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vocabulary image configuration'),
    '#weight' => 0,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['vocabulary']['vocabulary_image_extension'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed file extensions'),
    '#description' => 'Separate extensions with a space or comma and do not include the leading dot.',
    '#default_value' => variable_get('vocabulary_image_extension'),
  );

  $form['vocabulary']['vocabulary_image_max_image_resolution'] = array(
    '#type' => 'item',
    '#title' => t('Maximum image resolution'),
  );
  $form['vocabulary']['vocabulary_image_max_image_resolution_width'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#maxlength' => 4,
    '#prefix' => '<div class="container-inline">',
    '#suffix' => 'x',
    '#default_value' => variable_get('vocabulary_image_max_image_resolution_width'),
  );
  $form['vocabulary']['vocabulary_image_max_image_resolution_height'] = array(
    '#type' => 'textfield',
    '#description' => '<br/>' . t('The maximum allowed image size expressed as WIDTHxHEIGHT (e.g. 640x480). Leave blank for no restriction. If a larger image is uploaded, it will be resized to reflect the given width and height. Resizing images on upload will cause the loss of EXIF data in the image.'),
    '#size' => 5,
    '#maxlength' => 4,
    '#suffix' => '</div><br/>',
    '#field_suffix' => 'pixels',
    '#default_value' => variable_get('vocabulary_image_max_image_resolution_height'),
  );

  $form['vocabulary']['vocabulary_image_min_image_resolution'] = array(
    '#type' => 'item',
    '#title' => t('Minimum image resolution'),
  );
  $form['vocabulary']['vocabulary_image_min_image_resolution_width'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#maxlength' => 4,
    '#prefix' => '<div class="container-inline">',
    '#suffix' => 'x',
    '#default_value' => variable_get('vocabulary_image_min_image_resolution_width'),
  );
  $form['vocabulary']['vocabulary_image_min_image_resolution_height'] = array(
    '#type' => 'textfield',
    '#description' => t('<br/>The minimum allowed image size expressed as WIDTHxHEIGHT (e.g. 640x480). Leave blank for no restriction. If a smaller image is uploaded, it will be rejected.'),
    '#size' => 5,
    '#maxlength' => 4,
    '#suffix' => '</div>',
    '#field_suffix' => 'pixels',
    '#default_value' => variable_get('vocabulary_image_min_image_resolution_height'),
  );

  $form['vocabulary']['vocabulary_image_max_upload_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum upload size'),
    '#description' => 'Enter a value like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes) in order to restrict the allowed file size. If left empty the file sizes will be limited only by PHP\'s maximum post and file upload sizes (current limit 2 MB).',
    '#default_value' => variable_get('vocabulary_image_max_upload_size'),
  );

  $form['vocabulary']['vocabulary_image_default_vocab_image'] = array(
    '#title' => t('Default image'),
    '#type' => 'managed_file',
    '#default_value' => variable_get('vocabulary_image_default_vocab_image'),
    '#upload_location' => 'public://vocab_image/',
  );

  if (variable_get('vocabulary_image_default_vocab_image')) {
    $image = file_load(variable_get('vocabulary_image_default_vocab_image'));
    $form['vocabulary']['default_image'] = array(
      '#markup' => theme('image_style', array('path' => $image->uri, 'style_name' => 'thumbnail')),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  return $form;
}
/**
 * Helper function for admin settings form validation.
 */
function vocabulary_image_settings_form_validate($form, &$form_state) {
  if (!empty($form_state['values']['vocabulary_image_extension']) && (preg_match('/\./', $form_state['values']['vocabulary_image_extension']))) {
    form_set_error('vocabulary_image_extension', t('Do not include the leading dot.'));
  }
  if (!empty($form_state['values']['vocabulary_image_max_image_resolution_width']) && !(is_numeric($form_state['values']['vocabulary_image_max_image_resolution_width']))) {
    form_set_error('vocabulary_image_max_image_resolution_width', t('Enter valid width for "Maximum image resolution".'));
  }
  if (!empty($form_state['values']['vocabulary_image_max_image_resolution_height']) && !(is_numeric($form_state['values']['vocabulary_image_max_image_resolution_height']))) {
    form_set_error('vocabulary_image_max_image_resolution_height', t('Enter valid height for "Maximum image resolution".'));
  }

  if (!empty($form_state['values']['vocabulary_image_min_image_resolution_width']) && !(is_numeric($form_state['values']['vocabulary_image_min_image_resolution_width']))) {
    form_set_error('vocabulary_image_min_image_resolution_width', t('Enter valid width for "Minimum image resolution".'));
  }
  if (!empty($form_state['values']['vocabulary_image_max_image_resolution_height']) && !(is_numeric($form_state['values']['vocabulary_image_min_image_resolution_height']))) {
    form_set_error('vocabulary_image_min_image_resolution_height', t('Enter valid height for "Minimum image resolution".'));
  }
}
/**
 * Helper function for admin settings form submission.
 */
function vocabulary_image_settings_form_submit($form, &$form_state) {
  if (isset($form_state['values'])) {
    foreach ($form_state['values'] as $key => $val) {
      if ($key == 'vocabulary_image_default_vocab_image') {
        if ($val) {
          // Load the file via file.fid.
          $file = file_load($val);
          // Change status to permanent.
          $file->status = FILE_STATUS_PERMANENT;
          // Save.
          file_save($file);
          file_usage_add($file, $key, $key, $file->fid);
        }
        if ($val) {
          $fid = $val;
          variable_set($key, $fid);
        }
      }
      else {
        variable_set($key, $val);
      }
    }
    drupal_set_message(t('Your configuration options have been saved.'));
  }
}
